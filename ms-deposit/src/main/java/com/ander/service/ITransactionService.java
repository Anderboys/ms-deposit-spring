package com.ander.service;


import com.ander.domain.Transaction;
import com.ander.domain.TransactionRedis;

import java.util.Map;

public interface ITransactionService {
	
	public Transaction save(Transaction transaction);	
	
	// Metodos Redis
	public TransactionRedis findByIdRedis(String id);
	public void save(TransactionRedis transaction);
	public Map<String, TransactionRedis> findAll();
    void update (TransactionRedis transaction);
    void delete (String id );


}
