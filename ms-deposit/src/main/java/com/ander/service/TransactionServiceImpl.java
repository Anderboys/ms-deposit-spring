package com.ander.service;

import com.ander.dao.ITransactionDao;
import com.ander.dao.ITransactionRedis;
import com.ander.domain.Transaction;
import com.ander.domain.TransactionRedis;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.Map;

@Service
public class TransactionServiceImpl implements ITransactionService {


    // Inyectamos el Repositorio
    @Autowired
    private ITransactionDao _transactionDao;

    // IYECTAMOS REPOSITORIO REDIS
    @Autowired
    private ITransactionRedis transactionRedis ;

    // Iyectamos q es de tipo Transaction
    @Override
    @Transactional
    public Transaction save(Transaction transaction) {
        return _transactionDao.save(transaction);
    }

    
    // FOR REDIS
    @Override
    public TransactionRedis findByIdRedis(String id) {
        // TODO Auto-generated method stub
        return transactionRedis.findById(id);
    }

    @Override
    public void save(TransactionRedis transaction) {
        // TODO Auto-generated method stub
        transactionRedis.save(transaction);
    }

    @Override
    public Map<String, TransactionRedis> findAll() {
        // TODO Auto-generated method stub
        return transactionRedis.findAll();
    }


    @Override
    public void update(TransactionRedis transaction) {
        // TODO Auto-generated method stub
        save (transaction);
    }

    @Override
    public void delete(String id) {
        // TODO Auto-generated method stub
    	transactionRedis.delete(id);
    }

}
