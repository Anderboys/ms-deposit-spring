package com.ander.controller;
import com.ander.domain.Transaction;
import com.ander.producer.DepositEventProducer;
import com.ander.dao.TransactionRedisDaoImpl;
import com.ander.service.ITransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import com.fasterxml.jackson.core.JsonProcessingException;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ander.domain.TransactionRedis;

@RestController
@RequestMapping("/deposit")
public class DepositController {
	
    @Autowired
    private ITransactionService service;
    
    @Autowired
    DepositEventProducer eventProducer;
    
    // IYECCION PARA REDIS
    @Autowired
    private TransactionRedisDaoImpl  transactionRedisImpl; 
    
    
    private Logger log = LoggerFactory.getLogger(DepositController.class);

    @PostMapping("depositevent")
    public ResponseEntity<Transaction> postDepositEvent(@RequestBody Transaction transaction) throws JsonProcessingException {
       
    	log.info("antes de transql");
    	
    	//1. GUARDAR ENVIO REST DEPOSITO
        Transaction transql = service.save(transaction);
        
        
        //log.info("despues de transql");
        log.info("despues de:  service.save(transaction);");
        log.info("antes de sendDepositEvent");
        
        //2. ENVIAR y GUARDAR EVENTO PRODUCER EN FAFKA topic = "transaction-events";
        eventProducer.sendDepositEvent(transql);
        
        log.info("despues de sendDepositEvent");
        return ResponseEntity.status(HttpStatus.CREATED).body(transql);
    }   
    
    // REDIS    
    
    //Listar all
    @GetMapping("/listar")
    public Map<String , TransactionRedis> getAll(){
        return transactionRedisImpl.findAll();
    } 
    // buscar por ID
    @GetMapping("/listar/{id}")
    public TransactionRedis findById (@PathVariable String id){
        return transactionRedisImpl.findById(id);
    }    
    // actualizar
    @PostMapping("/update")
    public void updateDeposit(@RequestBody TransactionRedis transaction) {
    	transactionRedisImpl.update(transaction);
    }    
    // registrar
    @PostMapping("/registrar")
    public void createDeposit(@RequestBody TransactionRedis transaction) {
    	transactionRedisImpl.save(transaction);
    }       
    // eliminar por ID
    @DeleteMapping("eliminar/{id}")
    public void deleteDeposito(@PathVariable String id) {
    	transactionRedisImpl.delete(id);
    }
    
    
    
    

}
