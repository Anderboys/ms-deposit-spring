package com.ander.dao;
import com.ander.domain.TransactionRedis;

import java.util.Map;


public interface ITransactionRedis {
	
    void save (TransactionRedis transaction);
    Map<String, TransactionRedis> findAll();
    TransactionRedis findById(String id );
    void update (TransactionRedis transaction);
    void delete (String id );
}
