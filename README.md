# ms-deposit

- **Ide: Spring Tool Suite 4** 
- Version: 4.11.0.RELEASE
- java Buil Path 11.0.5
- pom.xml  <version>2.6.10</version>
- <java.version>11</java.version>
- Docker file  
**Proyecto listo para dokerizar** 
```
FROM openjdk:11.0.15-jdk
COPY "./target/ms-deposit.jar" "ms-deposit-app.jar"
EXPOSE 8006
ENTRYPOINT ["java","-jar","ms-deposit-app.jar"]
```
